#include "Level.h"
#include "WindowHandler.h"

Level::Level(std::string filepath) {
	loadMap(filepath);
	createWalls();
}

/* 
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space.
2 represents Pac Mans start position.
*/
void Level::loadMap(std::string filepath) {
	int x, y, temp;
	FILE* file = fopen(filepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &x, &y);

	for(int i = 0; i<y; i++) {
		std::vector<int> row;
		for(int j = 0; j<x; j++) {
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createWalls() {
	glm::vec2 pos, size;
	int rows = map.size();
	int cells = 0;

	for(int i = 0; i<rows; i++) {
		cells = map[i].size();

		for(int j = 0; j<cells; j++) {
			if(map[i][j] == 1) {
				ScreenObject wall;
				// TODO
				walls.push_back(wall);
			}
		}
	}
}

std::vector<ScreenObject>* Level::getWalls() {
	return &walls;
}