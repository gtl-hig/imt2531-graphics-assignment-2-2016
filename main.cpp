#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>

#include "globals.h"

#include "InputHandler.h"
#include "WindowHandler.h"
#include "ShaderHandler.h"
#include "ModelHandler.h"

#include "Level.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Movable.h"
#include "Camera.h"


//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels() {
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++) {
		char tempCString[51];
		fscanf(file, "%50s", &tempCString);
		tempString = tempCString;
		Level level(tempString);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;
	
	return &gLevels.front();
}

bool initGL()
{
	//Success flag
	bool success = true;

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	return success;
}

/**
 * Initializes the InputHandler, WindowHandler and OpenGL
 */
bool init() {
	InputHandler::getInstance().init();

	if(!WindowHandler::getInstance().init()) {
		gRunning = false;
		return false;
	}

	initGL();

	return true;
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue) {
	GameEvent nextEvent;
	while(!eventQueue.empty()) {
		nextEvent = eventQueue.front();
		eventQueue.pop();

		if(nextEvent.action == ActionEnum::PLAYER_MOVE_UP){
		}
	}
}

//All draw calls should originate here
void draw(Level* currentLevel, Camera* camera, ShaderHandler::ShaderProgram* shaderProgram) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	//Update screen
	SDL_GL_SwapWindow(WindowHandler::getInstance().getWindow());
}

//Calls cleanup code on program exit.
void close() {
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[]) {
	float nextFrame      = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime      = 0.0f; //Time since last pass through of the game loop.
	auto clockStart      = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop       = clockStart;

	ShaderHandler::ShaderProgram* currentShaderProgram = nullptr;

	init();

	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	
	Camera mainCamera(glm::vec3(0.0f, 100.0f, 0.0f));
	
	ModelHandler modelHandler;
	modelHandler.loadModelData("cube", "./resources/models/cube.model");

	ShaderHandler shaderHandler;
	currentShaderProgram = shaderHandler.initializeShaders();

	Level* currentLevel = loadLevels();


	while(gRunning) {
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue);

		if(nextFrameTimer >= nextFrame) {
			draw(currentLevel, &mainCamera, currentShaderProgram);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}