#include "Movable.h"


/**
 * [Movable::move description]
 * @param collidables [description]
 */
void Movable::move(std::vector<ScreenObject*> collidables) {
	//TODO
	//Assume that collision detection has already taken place by this point.
	//Applying the current m_movementDirection and m_speed will not result in a collision.
	//Add your code below this comment.
}

/**
 * This should rotate the object so that it faces in a specific direction.
 * This will be usefull with the spotlight.
 * @param direction The new direction
 */
void rotate(glm::vec3 direction) {
	//TODO
}

/*
bool Movable::checkCollisionWith(SDL_Rect hitbox, SDL_Rect* intersection) {
	if (SDL_IntersectRect(&m_collisionRect, &hitbox, intersection) == SDL_TRUE) {
		return true;
	} else {
		return false;
	}
}

void Movable::moveCollisionRect() {
	m_collisionRect.x += m_movementDirection.x * m_speed;
	m_collisionRect.x += m_movementDirection.z * m_speed;
}
*/

void Movable::setDirection(Movable::direction direction) {
	m_oldMovementDirection = m_movementDirection;

	switch(direction) {
		case(UP):
			m_movementDirection = glm::vec3(0.0f, 0.0f, 1.0f);
			break;
		case(LEFT):
			m_movementDirection = glm::vec3(-1.0f, 0.0f, 0.0f);
			break;
		case(DOWN):
			m_movementDirection = glm::vec3(0.0f, 0.0f, -1.0f);
			break;
		case(RIGHT):
			m_movementDirection = glm::vec3(1.0f, 0.0f, 0.0f);
			break;
		default:
			printf("Error!: The direction is not valid.\n");
	}
}

void Movable:: setSpeed(float speed) {
	m_speed = speed;
}