# Assignment 2

## Group creation deadline 2016/11/11 23:59:59
## Hand in deadline 2016/11/25 23:59:59

In this assignment you will be making Pac Man in 3D.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

There has been som miscomunication between Simon myself and the examination office. The assignments are supposed to be 40% of your grade (not 60% as I though). As such this assignment is counting towards 20% of your grade.  
You will have 3 weeks to finish this assignment.


## Required work
2. Finish the Level::createWalls() function so that walls are created. (The walls should be in 3D)
  2. In addition add a floor to the level.
1. Finish ModelHandler::createModel()
  2. You might wish to change the Model struct.
3. Add textures to the walls. (Any kind of texture will do as long as it is visible. Preferably not yellow.)
3. Finish the Camera class.
  3. The camera should be placed at a 60% angle to the level with a perspective view.
4. Finish the Movable::move() function.
  5. Move over any changes you made to the InputHandler in assignment 1 that you think you will need.
5. Create a pacman object from the Movable class or a subclass thereof. (It is ok if pacman is a cube.)
  5. Pacman should be controllable using WASD. He should be able to move in 2D. (You might want to change the Movable::setDirection() function to be more intuative for you.)
  5. Pacman should be yellow
6. Give the packman object a light. The light should have a yellow/redish color. Like from a torch.
  3. Add different kinds of light so that pacman can switch between a pointlight and a spotlight pointing forward by pressing the E key.
7. Create shaders that draw the map in darkness unless pacman is there with his light. The lighting should be full phong lighting and take the color of pacmans light into account. (There is no need to implement shadows. The light can go through the walls.)
2. The code should compile and run on Linux.
1. Update this document. (See bottom.)

## Notes
1. I forgot I promised to add collision to the skeleton code. I'll finish it by the end of the weekend. You are welcome to make your own if you don't want to wait. **Collision code can now be found on the collision branche.**
2. If you have the same issue in your code that exist in the labs where the code will produce a segmentation fault unless being run in debug mode add a comments about that. There will be no penalty if this is the case.
3. The base code has not been tested on Windows.
4. If there are any bugs in the code or other issues please send me an e-mail about it. I have not added any bugs on purpos.

## Suggestions for additional work
1. Implement loding of OBJ files and load and use a model from an OBJ file.
2. Implement a way to move the camera.
4. Port over features from assignment 1. (This will not give as much credit as in asignment 1 unless heavy modification was needed. Add comments about that you needed to change below.)
6. Add a way to switch between perspective and orthographic projection.
3. Add shadows.

##Group comments
###Who are the members of the group?###
###What did you implement and how did you do it? (Individually)###
###What parts if any of the base code did you change and why?###
###What was the hardest part of this assignment? (Individually)###
###Did you feel like the assignment was an appropriate ammount of work? (Individually)###
###What additional features if any did you add?###
###Are there any keybindings I should be aware of ouside of WASD?###
###Other comments###
