#include "ShaderHandler.h"

const std::string shaderPath = "./resources/shaders/";

ShaderHandler::ShaderProgram* ShaderHandler::initializeShaders()
{
	this->shaders["Red"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("Red"));

	return this->shaders["Red"].get(); 
}

ShaderHandler::ShaderProgram* ShaderHandler::getShader(const std::string& shader)
{
	auto it = this->shaders.find(shader);
	if (it != this->shaders.end())
	{
		return this->shaders[shader].get();
	}

	return nullptr;
}

ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";

	this->programId = LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str());
	
	this->MVPId = glGetUniformLocation(this->programId, "MVP");
	this->viewMatrixId = glGetUniformLocation(this->programId, "ViewMatrix");
	this->modelMatrixId = glGetUniformLocation(this->programId, "ModelMatrix");

	this->textureId = glGetUniformLocation(this->programId, "textureBuffer");

	this->lightPositionId = glGetUniformLocation(this->programId, "LightPosition_worldspace");

	this->cameraPositionId = glGetUniformLocation(this->programId, "CameraPosition_worldspace");

	glUseProgram(this->programId);
}

ShaderHandler::ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(this->programId);
}