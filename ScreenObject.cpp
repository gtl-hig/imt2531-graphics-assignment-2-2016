#include "ScreenObject.h"

ScreenObject::ScreenObject(Model* model, glm::vec3 position) {
	m_model = model;
	m_position = position;
	m_modelMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(1.0f));
}

void ScreenObject::draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram) {
	glm::mat4 MVPMatrix = camera->getProjectionMatrix() * camera->getViewMatrix() * m_modelMatrix;

	//TODO
}

void ScreenObject::update() {

}

//GET/////////////////////////////////////////
SDL_Rect ScreenObject::getCollisionRect() {
	return m_collisionRect;
}

glm::mat4 ScreenObject::getModelMatrix() {
	return m_modelMatrix;
}

//SET/////////////////////////////////////////
void ScreenObject::setModel(Model* model) {
	m_model = model;
}

void ScreenObject::setModelMatrix(glm::mat4 modelMatrix) {
	m_modelMatrix = modelMatrix;
}

void ScreenObject::setPosition(glm::vec3 position) {
	m_position = position;
	m_modelMatrix = glm::translate(glm::mat4(1.0f), position);
	setCollisionRect(position, glm::vec2(m_collisionRect.w, m_collisionRect.h));
}

void ScreenObject::setCollisionRect(glm::vec3 position,	glm::vec2 size) {
	SDL_Rect newCollisionRect;
	newCollisionRect.x = position.x;
	newCollisionRect.y = position.z;
	newCollisionRect.w = size.x;
	newCollisionRect.h = size.y;

	m_collisionRect = newCollisionRect;
}