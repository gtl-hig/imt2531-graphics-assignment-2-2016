#pragma once

#include <vector>

#include "ScreenObject.h"
#include "ModelHandler.h"

class Movable : public ScreenObject {
public:
	enum direction {
		UP,
		LEFT,
		DOWN,
		RIGHT
	};

	Movable() {};
	Movable(Model* model, glm::vec3 position) : ScreenObject(model, position) {};
	~Movable() {};

	void move(std::vector<ScreenObject*> collidables);
	void rotate(glm::vec3 direction);
	//bool checkCollisionWith(SDL_Rect hitbox, SDL_Rect* intersection);

	void setDirection(Movable::direction direction);
	void setSpeed(float speed);

private:
	glm::vec3 m_movementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 m_oldMovementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	float m_speed = 0.0f;

	//void moveCollisionRect();
};