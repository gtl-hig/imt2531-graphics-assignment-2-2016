#include "Camera.h"
#include "WindowHandler.h"

Camera::Camera(glm::vec3 position) {
	m_position 			= position;

	lookAt();
	setProjection(0.1f, 200.0f);
}

Camera::~Camera(){

}

/**
 * Sets the viewMatrix for the camera
 * @param position 	The position the camera should look at
 * @param up  		The up direction
 */
void Camera::lookAt(glm::vec3 position, glm::vec3 up) {
	//TODO
}

/**
 * Sets the position of the camera
 * @param position 
 */
void Camera::setPosition(glm::vec3 position) {
	m_position = position;
}

/**
 * Sets the perspective matrix for the camera
 * @param near Near clip plane
 * @param far  Far clip plane
 * @param fov  Field of view angle in radians
 */
void Camera::setProjection(float near, float far, float fov) {
	//TODO
}

glm::mat4 Camera::getViewMatrix() {
	return m_viewMatrix;
}

glm::mat4 Camera::getProjectionMatrix() {
	return m_projectionMatrix;
}