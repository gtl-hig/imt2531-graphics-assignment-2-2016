#include "ModelHandler.h"

/**
 * Loads model data from file.
 * @param  name The key for accessing the model from the model map.
 * @param  path Path to file.
 * @return      Success.
 */
bool ModelHandler::loadModelData(std::string name, std::string path) {
	int numberOfVertexes;
	int vertexOrder;
	int colorDepth;
	int textureOrder;
	int normalOrder;
	int vertexDataPoints;
	int numberOfIndices;
	int drawMode;
	

	FILE* file = fopen(path.c_str(), "r");
	if(file == NULL) {
		printf("Error!: Could not open the file: %s\n", path.c_str());
		return false;
	}

	fscanf(file, " %d,", &numberOfVertexes);
	fscanf(file, " %d,", &vertexOrder);
	fscanf(file, " %d,", &colorDepth);
	fscanf(file, " %d,", &textureOrder);
	fscanf(file, " %d,", &normalOrder);

	vertexDataPoints = 	numberOfVertexes*vertexOrder+
						numberOfVertexes*colorDepth+
						numberOfVertexes*textureOrder+
						numberOfVertexes*normalOrder;

	GLfloat vertexData[vertexDataPoints];
	for (int i = 0; i<vertexDataPoints; i++) {
		fscanf(file, " %f", &vertexData[i]);
	}

	fscanf(file, " %d,", &numberOfIndices);

	GLuint indexData[numberOfIndices];
	for (int i = 0; i<numberOfIndices; i++) {
		fscanf(file, " %d", &indexData[i]);
	}

	fscanf(file, " %d,", &drawMode);

	fclose(file);
	file = nullptr;

	Model model = createModel(drawMode);
	
	auto response = m_models.insert(std::pair<std::string,Model>(name,model));

	if(response.second == false) {
		printf("Error!: A model by the name of %s already exists.\n", name.c_str());
		return false;
	}

	return true;
}

/**
 * Creates VAO, VBO, IBO and drawMode for the model and store them in a Model struct
 * @param  drawMode			An int representing the OpenGL draw mode.
 * @return                  The finished Model struct
 */
Model ModelHandler::createModel(int drawMode) {
	Model model;

	switch(drawMode) {
		case(0)	:	model.drawMode = GL_TRIANGLES;	break;
		default: 
			printf("Error!: Draw mode not recognized.\n");
			break;
	}

	//Do things.
	//You will need more information than what is currently accessible from this function.

	return model;
}

Model* ModelHandler::getModel(std::string name) {
	if(m_models.count(name)) {
		return &m_models[name];
	} else {
		printf("Error!: No model by name %s could be found.\n", name.c_str());
		return nullptr;
	}
}